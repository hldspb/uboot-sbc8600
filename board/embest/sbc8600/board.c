/*
 * board.c
 *
 * Board functions for TI AM335X based boards
 *
 * Copyright (C) 2011, Texas Instruments, Incorporated - http://www.ti.com/
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#include <common.h>
#include <errno.h>
#include <libfdt.h>
#include <spl.h>
//#include <asm/arch/cpu.h>
#include <asm/arch/hardware.h>
//#include <asm/arch/omap.h>
#include <asm/arch/ddr_defs.h>
#include <asm/arch/clock.h>
#include <asm/arch/gpio.h>
#include <asm/arch/mmc_host_def.h>
#include <asm/arch/sys_proto.h>
#include <asm/arch/mem.h>
#include <asm/arch/mux.h>
#include <asm/io.h>
#include <asm/emif.h>
#include <asm/gpio.h>
#include <i2c.h>
#include <miiphy.h>
#include <cpsw.h>
#include <power/tps65217.h>
#include <environment.h>
#include <watchdog.h>
#include <linux/usb/ch9.h>
#include <linux/usb/gadget.h>
#include <linux/usb/musb.h>
#include <asm/omap_musb.h>
//#include <asm/arch-omap3/omap.h>
//#include <asm/arch-omap3/cpu.h>
#include <configs/ti_am335x_common.h>
#include "board.h"

#ifdef CONFIG_USB_EHCI_HCD
#include <usb.h>
#include <asm/ehci-omap.h>
#endif




DECLARE_GLOBAL_DATA_PTR;

/* GPIO that controls power to DDR on EVM-SK */
#define GPIO_DDR_VTT_EN		7
#define DIP_S1			44
#define MPCIE_SW		100


static struct ctrl_dev *cdev = (struct ctrl_dev *)CTRL_DEVICE_BASE;


static int read_eeprom(BSP_VS_HWPARAM *header)
{
	i2c_set_bus_num(1);

	/* Check if baseboard eeprom is available */
	if (i2c_probe(CONFIG_SYS_I2C_EEPROM_ADDR)) {
		puts("Could not probe the EEPROM; something fundamentally "
			"wrong on the I2C bus.\n");
		return -ENODEV;
	}

	/* read the eeprom using i2c */
	if (i2c_read(CONFIG_SYS_I2C_EEPROM_ADDR, 0, 1, (uchar *)header,
		     sizeof(BSP_VS_HWPARAM))) {
		puts("Could not read the EEPROM; something fundamentally"
			" wrong on the I2C bus.\n");
		return -EIO;
	}

	if (header->Magic != 0xDEADBEEF) {

		printf("Incorrect magic number (0x%x) in EEPROM\n",
				header->Magic);

		/* fill default values */
		header->SystemId = 211;
		header->MAC1[0] = 0x00;
		header->MAC1[1] = 0x00;
		header->MAC1[2] = 0x00;
		header->MAC1[3] = 0x00;
		header->MAC1[4] = 0x00;
		header->MAC1[5] = 0x01;

		header->MAC2[0] = 0x00;
		header->MAC2[1] = 0x00;
		header->MAC2[2] = 0x00;
		header->MAC2[3] = 0x00;
		header->MAC2[4] = 0x00;
		header->MAC2[5] = 0x02;

		header->MAC3[0] = 0x00;
		header->MAC3[1] = 0x00;
		header->MAC3[2] = 0x00;
		header->MAC3[3] = 0x00;
		header->MAC3[4] = 0x00;
		header->MAC3[5] = 0x03;
	}

	return 0;
}

#if defined(CONFIG_SPL_BUILD) || defined(CONFIG_NOR_BOOT)

static const struct ddr_data ddr3_baltos_data = {
	.datardsratio0 = MT41K256M16HA125E_RD_DQS,
	.datawdsratio0 = MT41K256M16HA125E_WR_DQS,
	.datafwsratio0 = MT41K256M16HA125E_PHY_FIFO_WE,
	.datawrsratio0 = MT41K256M16HA125E_PHY_WR_DATA,
};

static const struct cmd_control ddr3_baltos_cmd_ctrl_data = {
	.cmd0csratio = MT41K256M16HA125E_RATIO,
	.cmd0iclkout = MT41K256M16HA125E_INVERT_CLKOUT,

	.cmd1csratio = MT41K256M16HA125E_RATIO,
	.cmd1iclkout = MT41K256M16HA125E_INVERT_CLKOUT,

	.cmd2csratio = MT41K256M16HA125E_RATIO,
	.cmd2iclkout = MT41K256M16HA125E_INVERT_CLKOUT,
};

static struct emif_regs ddr3_baltos_emif_reg_data = {
	.sdram_config = MT41K256M16HA125E_EMIF_SDCFG,
	.ref_ctrl = MT41K256M16HA125E_EMIF_SDREF,
	.sdram_tim1 = MT41K256M16HA125E_EMIF_TIM1,
	.sdram_tim2 = MT41K256M16HA125E_EMIF_TIM2,
	.sdram_tim3 = MT41K256M16HA125E_EMIF_TIM3,
	.zq_config = MT41K256M16HA125E_ZQ_CFG,
	.emif_ddr_phy_ctlr_1 = MT41K256M16HA125E_EMIF_READ_LATENCY,
};

#ifdef CONFIG_SPL_OS_BOOT
int spl_start_uboot(void)
{
	/* break into full u-boot on 'c' */
	return (serial_tstc() && serial_getc() == 'c');
}
#endif

#define OSC	(V_OSCK/1000000)
const struct dpll_params dpll_ddr = {
		266, OSC-1, 1, -1, -1, -1, -1};
const struct dpll_params dpll_ddr_evm_sk = {
		303, OSC-1, 1, -1, -1, -1, -1};
const struct dpll_params dpll_ddr_baltos = {
		400, OSC-1, 1, -1, -1, -1, -1};

// ol: taken from sl50
void am33xx_spl_board_init(void)
{
        int mpu_vdd;

        /* Get the frequency */
        dpll_mpu_opp100.m = am335x_get_efuse_mpu_max_freq(cdev);
	printf("Max MPU freq is: %u MHz\n", (unsigned int)dpll_mpu_opp100.m);

        //ol: use i2c bus 0
        i2c_set_bus_num(0);
        i2c_init(CONFIG_SYS_I2C_SPEED, CONFIG_SYS_I2C_SLAVE);

        printf("I2C speed: %d Hz\n", CONFIG_SYS_I2C_SPEED);


	//ol: this kills ethernet!!!
        /* Set CORE Frequencies to OPP100 */
        //do_setup_dpll(&dpll_core_regs, &dpll_core_opp100);

        /* Set MPU Frequency to what we detected now that voltages are set */
        //do_setup_dpll(&dpll_mpu_regs, &dpll_mpu_opp100);
	//ol: try to get params dynamically
	do_setup_dpll(&dpll_mpu_regs, get_dpll_mpu_params());

	//ol: what's this? taken from Baltos
	//writel(0x000010ff, PRM_DEVICE_INST + 4);

        /* BeagleBone PMIC Code */
        int usb_cur_lim;

        if (i2c_probe(TPS65217_CHIP_PM))
	{
		printf("i2c_probe(TPS65217_CHIP_PM) failed! Quitting am33xx_spl_board_init()\n");
                return;
	}

	/****************************************************************************************/

        /*
         * Increase USB current limit to 1300mA or 1800mA and set
         * the MPU voltage controller as needed.
         */
        if (dpll_mpu_opp100.m == MPUPLL_M_1000) {
                usb_cur_lim = TPS65217_USB_INPUT_CUR_LIMIT_1800MA;
                mpu_vdd = TPS65217_DCDC_VOLT_SEL_1325MV;
        } else {
                usb_cur_lim = TPS65217_USB_INPUT_CUR_LIMIT_1300MA;
                mpu_vdd = TPS65217_DCDC_VOLT_SEL_1275MV;
        }

        if (tps65217_reg_write(TPS65217_PROT_LEVEL_NONE,
                               TPS65217_POWER_PATH,
                               usb_cur_lim,
                               TPS65217_USB_INPUT_CUR_LIMIT_MASK))
                puts("tps65217_reg_write failure\n");

        /* Set DCDC3 (CORE) voltage to 1.125V */
        if (tps65217_voltage_update(TPS65217_DEFDCDC3,
                                    TPS65217_DCDC_VOLT_SEL_1125MV)) {
                puts("tps65217_voltage_update failure\n");
                return;
        }

        /* Set CORE Frequencies to OPP100 */
        do_setup_dpll(&dpll_core_regs, &dpll_core_opp100);

        /* Set DCDC2 (MPU) voltage */
        if (tps65217_voltage_update(TPS65217_DEFDCDC2, mpu_vdd)) {
                puts("tps65217_voltage_update failure\n");
                return;
        }

        /*
         * Set LDO3 to 1.8V and LDO4 to 3.3V
         */
        if (tps65217_reg_write(TPS65217_PROT_LEVEL_2,
                               TPS65217_DEFLS1,
                               TPS65217_LDO_VOLTAGE_OUT_1_8,
                               TPS65217_LDO_MASK))
                puts("tps65217_reg_write failure\n");

        if (tps65217_reg_write(TPS65217_PROT_LEVEL_2,
                               TPS65217_DEFLS2,
                               TPS65217_LDO_VOLTAGE_OUT_3_3,
                               TPS65217_LDO_MASK))
                puts("tps65217_reg_write failure\n");

        /* Set MPU Frequency to what we detected now that voltages are set */
        do_setup_dpll(&dpll_mpu_regs, &dpll_mpu_opp100);

	printf("am33xx_spl_board_init() done!\n");
}


const struct dpll_params *get_dpll_ddr_params(void)
{
	enable_i2c1_pin_mux();
	i2c_set_bus_num(1);

	return &dpll_ddr_baltos;
}

void set_uart_mux_conf(void)
{
	enable_uart0_pin_mux();
}

void set_mux_conf_regs(void)
{
	enable_board_pin_mux();
}

const struct ctrl_ioregs ioregs_baltos = {
	.cm0ioctl		= MT41K256M16HA125E_IOCTRL_VALUE,
	.cm1ioctl		= MT41K256M16HA125E_IOCTRL_VALUE,
	.cm2ioctl		= MT41K256M16HA125E_IOCTRL_VALUE,
	.dt0ioctl		= MT41K256M16HA125E_IOCTRL_VALUE,
	.dt1ioctl		= MT41K256M16HA125E_IOCTRL_VALUE,
};

void sdram_init(void)
{
	gpio_request(GPIO_DDR_VTT_EN, "ddr_vtt_en");
	gpio_direction_output(GPIO_DDR_VTT_EN, 1);

	config_ddr(400, &ioregs_baltos,
		   &ddr3_baltos_data,
		   &ddr3_baltos_cmd_ctrl_data,
		   &ddr3_baltos_emif_reg_data, 0);
}
#endif






/*
 * Basic board specific setup.  Pinmux has been handled already.
 */
int board_init(void)
{
#if defined(CONFIG_HW_WATCHDOG)
	hw_watchdog_init();
#endif

	gd->bd->bi_boot_params = CONFIG_SYS_SDRAM_BASE + 0x100;
#if defined(CONFIG_NOR) || defined(CONFIG_NAND)
	gpmc_init();
#endif



	printf("board_init() done.\n");
	return 0;
}

int ft_board_setup(void *blob, bd_t *bd)
{

	//ol: we haev no eeprom, bail out
	//return 0;






	int node, ret;
	unsigned char mac_addr[6];
	BSP_VS_HWPARAM header;

	/* get production data */
	if (read_eeprom(&header))
		return 0;

	/* setup MAC1 */
	mac_addr[0] = header.MAC1[0];
	mac_addr[1] = header.MAC1[1];
	mac_addr[2] = header.MAC1[2];
	mac_addr[3] = header.MAC1[3];
	mac_addr[4] = header.MAC1[4];
	mac_addr[5] = header.MAC1[5];


	node = fdt_path_offset(blob, "/ocp/ethernet/slave@4a100200");
	if (node < 0) {
		printf("no /soc/fman/ethernet path offset\n");
		return -ENODEV;
	}

	ret = fdt_setprop(blob, node, "mac-address", &mac_addr, 6);
	if (ret) {
		printf("error setting local-mac-address property\n");
		return -ENODEV;
	}

	/* setup MAC2 */
	mac_addr[0] = header.MAC2[0];
	mac_addr[1] = header.MAC2[1];
	mac_addr[2] = header.MAC2[2];
	mac_addr[3] = header.MAC2[3];
	mac_addr[4] = header.MAC2[4];
	mac_addr[5] = header.MAC2[5];

	node = fdt_path_offset(blob, "/ocp/ethernet/slave@4a100300");
	if (node < 0) {
		printf("no /soc/fman/ethernet path offset\n");
		return -ENODEV;
	}

	ret = fdt_setprop(blob, node, "mac-address", &mac_addr, 6);
	if (ret) {
		printf("error setting local-mac-address property\n");
		return -ENODEV;
	}

	printf("\nFDT was successfully setup\n");

	return 0;
}


#ifdef CONFIG_BOARD_LATE_INIT
int board_late_init(void)
{

	//printf("board_late_init(void)\n");
#ifdef CONFIG_ENV_VARS_UBOOT_RUNTIME_CONFIG
	BSP_VS_HWPARAM header;
	char model[4];

	/* get production data */
	if (read_eeprom(&header)) {
		strcpy(model, "211");
	} else {
		sprintf(model, "%d", header.SystemId);
		if (header.SystemId == 215) {
			configure_module_pin_mux(dip_pin_mux);
			baltos_set_console();
		}
	}

	/* turn power for the mPCIe slot */
	configure_module_pin_mux(pcie_sw_pin_mux);
	if (gpio_request(MPCIE_SW, "mpcie_sw")) {
		printf("failed to export GPIO %d\n", MPCIE_SW);
		return -ENODEV;
	}
	if (gpio_direction_output(MPCIE_SW, 1)) {
		printf("failed to set GPIO %d direction\n", MPCIE_SW);
		return -ENODEV;
	}

	env_set("board_name", model);
#endif

	return 0;
}
#endif

#if (defined(CONFIG_DRIVER_TI_CPSW) && !defined(CONFIG_SPL_BUILD)) || \
	(defined(CONFIG_SPL_ETH_SUPPORT) && defined(CONFIG_SPL_BUILD))



static void cpsw_control(int enabled)
{
	/* VTP can be added here */

	return;
}

static struct cpsw_slave_data cpsw_slaves[] = {
        {
                .slave_reg_ofs  = 0x208,
                .sliver_reg_ofs = 0xd80,
                .phy_addr       = 4,
        },
        {
                .slave_reg_ofs  = 0x308,
                .sliver_reg_ofs = 0xdc0,
                .phy_addr       = 6,
        },
};

static struct cpsw_platform_data cpsw_data = {
        .mdio_base              = CPSW_MDIO_BASE,
        .cpsw_base              = CPSW_BASE,
        .mdio_div               = 0xff,
        .channels               = 8,
        .cpdma_reg_ofs          = 0x800,
        .slaves                 = 2,
        .slave_data             = cpsw_slaves,
        .ale_reg_ofs            = 0xd00,
        .ale_entries            = 1024,
        .host_port_reg_ofs      = 0x108,
        .hw_stats_reg_ofs       = 0x900,
        .bd_ram_ofs             = 0x2000,
        .mac_control            = (1 << 5),
        .control                = cpsw_control,
        .host_port_num          = 0,
        .version                = CPSW_CTRL_VERSION_2,
};

#endif

#if ((defined(CONFIG_SPL_ETH_SUPPORT) || defined(CONFIG_SPL_USBETH_SUPPORT)) \
		&& defined(CONFIG_SPL_BUILD)) || \
	((defined(CONFIG_DRIVER_TI_CPSW) || \
	  defined(CONFIG_USB_ETHER) && defined(CONFIG_USB_MUSB_GADGET)) && \
	 !defined(CONFIG_SPL_BUILD))
int board_eth_init(bd_t *bis)
{

	//printf("board_eth_init() starting...\n");
	int rv, n = 0;
	uint8_t mac_addr[6];
	uint32_t mac_hi, mac_lo;


	/* try reading mac address from efuse */
	mac_lo = readl(&cdev->macid0l);
	mac_hi = readl(&cdev->macid0h);
	mac_addr[0] = mac_hi & 0xFF;
	mac_addr[1] = (mac_hi & 0xFF00) >> 8;
	mac_addr[2] = (mac_hi & 0xFF0000) >> 16;
	mac_addr[3] = (mac_hi & 0xFF000000) >> 24;
	mac_addr[4] = mac_lo & 0xFF;
	mac_addr[5] = (mac_lo & 0xFF00) >> 8;

#if (defined(CONFIG_DRIVER_TI_CPSW) && !defined(CONFIG_SPL_BUILD)) || \
	(defined(CONFIG_SPL_ETH_SUPPORT) && defined(CONFIG_SPL_BUILD))
	if (!env_get("ethaddr")) {
		printf("<ethaddr> not set. Validating first E-fuse MAC\n");

		if (is_valid_ethaddr(mac_addr))
			eth_env_set_enetaddr("ethaddr", mac_addr);
	}

#ifdef CONFIG_DRIVER_TI_CPSW

        mac_lo = readl(&cdev->macid1l);
        mac_hi = readl(&cdev->macid1h);
        mac_addr[0] = mac_hi & 0xFF;
        mac_addr[1] = (mac_hi & 0xFF00) >> 8;
        mac_addr[2] = (mac_hi & 0xFF0000) >> 16;
        mac_addr[3] = (mac_hi & 0xFF000000) >> 24;
        mac_addr[4] = mac_lo & 0xFF;
        mac_addr[5] = (mac_lo & 0xFF00) >> 8;

        if (!env_get("eth1addr")) {
                if (is_valid_ethaddr(mac_addr))
                        eth_env_set_enetaddr("eth1addr", mac_addr);
        }


        writel(RGMII_MODE_ENABLE | RGMII_INT_DELAY, &cdev->miisel);
        cpsw_slaves[0].phy_if = cpsw_slaves[1].phy_if =
                                PHY_INTERFACE_MODE_RGMII_TXID;

        rv = cpsw_register(&cpsw_data);
        if (rv < 0)
                printf("Error %d registering CPSW switch\n", rv);
        else
                n += rv;

#endif


#endif

#if defined(CONFIG_USB_ETHER) && \
        (!defined(CONFIG_SPL_BUILD) || defined(CONFIG_SPL_USBETH_SUPPORT))
        if (is_valid_ethaddr(mac_addr))
                eth_env_set_enetaddr("usbnet_devaddr", mac_addr);

        rv = usb_eth_initialize(bis);
        if (rv < 0)
                printf("Error %d registering USB_ETHER\n", rv);
        else
                n += rv;
#endif

	//printf("board_eth_init() done.\n");
	return n;
}
#endif


#if 0

#ifdef CONFIG_USB_MUSB_OMAP2PLUS
static struct musb_hdrc_config musb_config = {
        .multipoint     = 1,
        .dyn_fifo       = 1,
        .num_eps        = 16,
        .ram_bits       = 12,
};

static struct omap_musb_board_data musb_board_data = {
        .interface_type = MUSB_INTERFACE_ULPI,
};

static struct musb_hdrc_platform_data musb_plat = {
#if defined(CONFIG_USB_MUSB_HOST)
        .mode           = MUSB_HOST,
#elif defined(CONFIG_USB_MUSB_GADGET)
        .mode           = MUSB_PERIPHERAL,
#else
#error "Please define either CONFIG_USB_MUSB_HOST or CONFIG_USB_MUSB_GADGET"
#endif
        .config         = &musb_config,
        .power          = 100,
        .platform_ops   = &omap2430_ops,
        .board_data     = &musb_board_data,
};
#endif










#if defined(CONFIG_USB_EHCI_HCD) && !defined(CONFIG_SPL_BUILD)
/* Call usb_stop() before starting the kernel */
void show_boot_progress(int val)
{
        if (val == BOOTSTAGE_ID_RUN_OS)
                usb_stop();
}

static struct omap_usbhs_board_data usbhs_bdata = {
        .port_mode[0] = OMAP_EHCI_PORT_MODE_PHY,
        .port_mode[1] = OMAP_EHCI_PORT_MODE_PHY,
        .port_mode[2] = OMAP_USBHS_PORT_MODE_UNUSED
};

int ehci_hcd_init(int index, enum usb_init_type init,
                struct ehci_hccr **hccr, struct ehci_hcor **hcor)
{
        printf("called ehci_hcd_init() for index %i\n", index);
        return omap_ehci_hcd_init(index, &usbhs_bdata, hccr, hcor);
}

int ehci_hcd_stop(int index)
{
        return omap_ehci_hcd_stop();
}
#endif /* CONFIG_USB_EHCI_HCD */

#endif /* if 0 */
